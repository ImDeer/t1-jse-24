package t1.dkhrunina.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.component.Bootstrap;

public final class Application {

    public static void main(@Nullable final String... args) {
        @NotNull
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}