package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "Show task by index.";

    @Override
    public void execute() {
        System.out.println("[Show task by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}