package t1.dkhrunina.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.model.ICommand;
import t1.dkhrunina.tm.api.service.IAuthService;
import t1.dkhrunina.tm.api.service.IServiceLocator;
import t1.dkhrunina.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @Getter
    @Setter
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public String getUserId() {
        return getAuthService().getUserId();
    }

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        final boolean hasName = !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();
        if (hasName) result += name;
        if (hasArgument) result += hasName ? (", " + argument) : argument;
        if (hasDescription) result += hasName || hasArgument ? (": " + description) : description;
        return result;
    }

}