package t1.dkhrunina.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String NAME = "about";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @Override
    public void execute() {
        System.out.println("\n[ABOUT]");
        System.out.println("Name: Daria Khrunina");
        System.out.println("Email: dkhrunina@t1-consulting.ru");
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}