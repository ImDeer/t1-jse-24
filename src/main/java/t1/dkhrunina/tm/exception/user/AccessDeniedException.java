package t1.dkhrunina.tm.exception.user;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error: you are not logged in, please try again");
    }

}